---
pagetitle: "2-Fundamentos de programación"
title: "Taller de R: Estadística y programación"
subtitle: "Lecture 2: Fundamentos de programación"
author: 
  name: Eduard Fernando Martínez González
  affiliation: Universidad de los Andes | [ECON-1302](https://bloqueneon.uniandes.edu.co/d2l/home/133092) #[`r fontawesome::fa('globe')`]()
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: darkblue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,kable,kableExtra,tidyverse,ggthemes,fontawesome)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--=================-->
<!--=================-->
## **1. Checklist**

Antes de iniciar con esta lectura asegúrese de...

#### **☑ Lectures previas**

Asegúrese de haber revisado la [Lecture 1: Introducción a R](https://lectures-r.gitlab.io/uniandes-202202/lecture-1/)

#### **☑ Versión de R**

Tener la versión `r R.version.string` instalada:

```{r, cache=T , echo=T}
R.version.string
```

<!--=================-->
<!--=================-->
## **2. Primeros pasos en R...**

Antes de empezar a trabajar con objetos, podría familiarizarse con algunas operaciones que puede correr/ejecutar y pintar/imprimir los resultados sobre la consola. Por ejemplo, en `R` se pueden ejecutar una serie de operaciones aritméticas.

#### **2.1 Operaciones aritméticas**

R reconoce todos los operadores aritméticos estándar:

```{r,echo=F}
my_tbl = tribble(
~`Operador aritmético`,~Descripción,
"`+`", "Suma",
"`-`", "Resta",
"`*`", "Multiplicación",
"`/`", "División",
"`^` , `**`", "Exponencial",
"`%%`", "Módulo",
"`%/%`", "División entera",
"`%*%`","Multiplicación matricial",
"`%o%`","Producto exterior",
"`%x%`","Producto Kronecker")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

Puede escribir la operación de suma/resta/producto sobre la consola y pintar los resultados sobre la consola:

```{r}
1+2 ## Suma
5/2 ## Division
2+4*1^3 ## Orden estándar de precedencia (por ejemplo: ejecuta `*` antes que `+`)
```

También podemos invocar operadores de módulo (división de enteros y resto):

```{r}
100 %/% 60 ## ¿Cuántas horas enteras hay en 100 minutos?
100 %% 60 ## ¿Cuántos minutos quedan?
```

#### **2.2 Operadores lógicos**

R también viene equipado con un conjunto completo de operadores lógicos y booleanos, que siguen el protocolo de programación estándar. Por ejemplo:

```{r,echo=F}
my_tbl = tribble(
~`Operador lógico`,~Descripción,
"< , >","Menor y mayor que...",
"<= , >=", "Menor o igual y mayor igual que..." ,
"`==`" , "Igual a..." ,
"`!=`" , "Diferente de..." ,
"&" , "y"  ,
"`|`" , "o"  ,
"`!`" , "Negación")

kbl(my_tbl) %>%
kable_paper("striped", full_width = F) %>%
row_spec(0, bold = T, color = "white", background = "#00008B")
```

Puede ejecutar operaciones lógicas sencillas o compuestas, por ejemplo:

```{r}
1 > 2 ## ¿Es 1 mayor qué 2?
1 > 2 & 1 > 0.5 ## ¿Es 1 mayor qué 2 y menor que 0.5? 
1 > 2 | 1 > 0.5 ## ¿Es 1 mayor qué 2 o menor que 0.5? 
isTRUE (1 < 2)
```

Al igual que la aritmética estándar, las declaraciones lógicas siguen un estricto orden de precedencia. Los operadores lógicos (`>`, `==`, etc.) se evalúan antes que los operadores booleanos (`&` y `|`). No reconocer esto puede conducir a un comportamiento inesperado...

```{r}
1 > 0.5 & 2 # 1 es mayor a 0.5 y mayor a 2?
```

**Solución:** Debe escribir el enunciado lógico de manera correcta:

```{r}
1 > 0.5 & 1 > 2
```

Usamos `!` como abreviatura de negación. Esto será muy útil cuando comencemos a filtrar objetos de datos en función de observaciones no faltantes (es decir, no NA).

```{r}
is.na(1:10)
!is.na(1:10)
# Negate(is.na)(1:10) ## This also works. Try it yourself.
```

#### **2.3. Evaluar coincidencias: `%in%`**

Para ver si un objeto está contenido dentro (es decir, coincide con uno de) una lista de elementos, use `%in%`.

```{r}
4 %in% 1:10
4 %in% 5:10
```

Pronto veremos como asignar objetos en R, por ahora veamos cómo aplicar una evaluación lógica para saber si un objeto es igual a otro. Para ello se emplea `==`:

```{r, error=T}
1 = 1 ## This doesn't work
1 == 1 ## This does.
1 != 2 ## Note the single equal sign when combined with a negation.
```

**Advertencia de evaluación:** números de coma flotante

¿Qué crees que pasará si evaluamos `0.1 + 0.2 == 0.3`?

```{r floating1}
0.1 + 0.2 == 0.3
```

**Problema:** Las computadoras representan números como puntos flotantes binarios (es decir, base 2). Más [aquí](https://floating-point-gui.de/basic/). Esto ayuda a obtener un resultado rápido y eficiente en memoria, pero puede provocar un comportamiento inesperado. Similar a la forma en que la representación decimal estándar (es decir, base 10) no puede capturar con precisión ciertas fracciones (por ejemplo, $\frac{1}{3} = 0.3333...$).

**Solución:** Use `all.equal()` para evaluar flotantes (es decir, fracciones).

```{r floating2}
all.equal(0.1 + 0.2, 0.3)
```

<!--=================-->
<!--=================-->

## **3. OP: Orientado a objetos**

#### **3.1 Asignar objetos**

Para asignar un objeto se debe seguir la siguiente estructura:

***nombre_objeto + asignar + funcion()***

```{bash eval=FALSE}
# Una forma de hacerlo: 
data_1 <- rnorm(n = 10 , mean = 50 , sd = 10)

# Otra forma de hacerlo: 
data_2 = rnorm(n = 10 , mean = 40 , sd = 10)
```

**¿Qué operador de asignación usar?**

La mayoría de los usuarios de R (¿puristas?) parecen preferir `<-` para la asignación, ya que `=` también tiene un papel específico para la evaluación *dentro* de las funciones.

-   Veremos muchos ejemplos de esto más adelante.

-   Pero no creo que importe; `=` es más rápido de escribir y es más intuitivo si vienes de otro lenguaje de programación. (Más discusión [aquí](https://github.com/Robinlovelace/geocompr/issues/319#issuecomment-427376764) y [aquí](https://www.separatinghyperplanes.com/2018/02/why-you-%20debería-usar-y-nunca.html).)

**Conclusión:** Usa lo que prefieras. Solo sé consistente dentro de tu propio código. i.e: Usa el mismo operador en la medida de lo posible.

También puede asignar objetos usando la siguiente estructura:

```{r , eval=T , include=T}
x1 <- 2 + 2
x1

assign("x2", 3/6)
x2

3*4 -> x3
x3
```

#### **3.2 ¿Qué es un objeto?**

Es importante enfatizar que hay muchos *tipos* (o *clases*) diferentes de objetos.

Volveremos a examinar el tema de "tipo" frente a "clase" en una o dos diapositivas. Por el momento, es útil simplemente nombrar algunos objetos con los que trabajaremos regularmente:

-   vectores
-   matrices
-   dataframes
-   listas
-   funciones
-   etc.

Cada clase de objeto tiene su propio conjunto de reglas ("métodos") para determinar operaciones válidas. Por ejemplo, aunque hay operaciones que funcionan en matrices y dataframes, también hay algunas operaciones que solo funcionan en una matriz, y viceversa. Al mismo tiempo, puede (generalmente) convertir un objeto de un tipo a otro.

```{r d}
## Crea un dataframe llamado "df".
df = data.frame(x = 1:2, y = 3:4)
df
```

```{m}
## Convertirlo en una matriz "mt".
mt = as.matrix(df)
mt
```

Puede inspeccionar/imprimir un objeto directamente en la consola debe escribir `df` en la consola o puede usar la función `View(df)` para verlo en el editor de datos.

#### **3.3 Clase, tipo y estructura**

Usar `class`, `typeof`, y `str` para conocer más acerca de un objeto.

```{r}
class(df) ## Evaluate its class.
typeof(df) ## Evaluate its type.
str(df) ## Show its structure.
```

#### **3.4 Todo tiene un 'nombre'**

Podemos asignar objetos a diferentes nombres. Sin embargo, hay una serie de palabras especiales que están "reservadas" en R. Estos son comandos, operadores y relaciones fundamentales en base R que no se pueden (re)asignar (aunque se quisiera). Consulte una lista completa [aquí](http://stat.ethz.ch/R-manual/R-devel/library/base/html/Reserved.html).

Algunas de las palabras que no se pueden usar:

``` r
if 
else 
while 
function 
for
TRUE 
FALSE 
NULL 
Inf 
NaN 
NA 
```

Además de la lista de palabras estrictamente reservadas, existe otro conjunto de palabras que, aunque se reasignar pero que es mejor no usarlas.

Por ejemplo: `c()` se usa para concatenar; es decir, crear vectores y unir diferentes objetos.

``` r
c = 4
c(1, 2, 5)
```

Afortunadamente nada. R es lo suficientemente "inteligente" para distinguir entre la variable `c = 4` que creamos y la función integrada `c()` que requiere concatenación.

¿Qué pasa si usamos comillas para nombrar un objeto?

``` r
"hola" = 2
hola
```

#### **3.5 ¿Workspace?**

El espacio de trabajo es la colección de objetos almacenados en la memoria activa de R. Para gestionar el espacio de trabajo puede:

```{r , eval=T , include=T}
objects() ## ver objetos en el espacio de trabajo
object.size(mtcars) ## peso de un objeto
ls() ## ver objetos en el espacio de trabajo
```

Puede remover uno o varios objetos del espacio de trabajo:

```{r , eval=T , include=T}
rm() ## remover un objeto del espacio de trabajo
rm(list=ls()) ## Limpiar el entorno de trabajo
cat("\f") ## Limpiar la consola
```

<!--=================-->
<!--=================-->
## **4. Líberias**

#### **4.1 Acerca de las librerías**:

- Las librerías son un conjunto/paquete de funciones especializadas en tareas específicas.

- Una librería se debe instalar en el equipo solo una vez -`install.packages()`-, pero se debe llamar/requerir -`library()` o `require()`- cada vez que se inicia una sesión de `Rstudio` en la que se va a utilizar alguna función de esa librería.

- Algunas librerías se actualizan constantemente, usted puede actualizar la versión de una librería usando la función `update.packages()`.

**Nota:** puede inspeccionar las librerías que tiene activas en su sesión corriendo la función `sessionInfo()` sobre la consola de R:

```{r , eval=F , out.height="20%" , }
## información de la sesión
sessionInfo()
```

```{r eval=F}
R version 4.1.1 (2021-08-10)
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Big Sur 10.16

Matrix products: default
BLAS:   /Library/Frameworks/R.framework/Versions/4.1/Resources/lib/libRblas.0.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/4.1/Resources/lib/libRlapack.dylib

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base   
```

#### **4.2 ¿Cómo gestionar las librerías en R?**

Cuando inicia sesión, R carga 7 librerías bases (`usted`,`stats`,`graphics`,`grDevices`,`utils`, `datasets, methods` y `base`). Pero usted puede instalar/llamar cerca de `r nrow(available.packages())` librerías disponibles en el CRAN de R.

**¿Como instalar/actualizar una librería?**

![](pics/install_lib.gif){width="529"}

<!--=================-->
<!--=================-->
## **5. Funciones y help**

#### **5.1 Funciones**

Una función permite crear, editar, transformar o eliminar objetos. Están contenidas en librerías. Y cada función contiene por lo menos 1 argumento.

![](pics/funciones.gif){width="529"}

#### **5.2 Obtener ayuda de una función**

Para obtener más información sobre una función u objeto (con nombre) en R, consulte la documentación de "help" o usando `?`:

```{r eval=T}
help(rnorm)
?rnorm 
```

**Hint:** Los comentarios en R están delimitados por `#`. Presiona `Ctrl+Shift+c` en RStudio para (des)comentar secciones completas del código resaltado.

**Hint:** ¿Ves la sección *Ejemplos* en la parte inferior del archivo de ayuda? Puedes ejecutarlos con la función `example()`.

```{r eval=F}
example(rnorm)
```

#### **5.3 Vignettes**

Para muchos paquetes, también puede probar la función `vignette()`, que proporcionará una introducción a un paquete y su propósito a través de una serie de ejemplos útiles. Una complicación es que necesita saber el nombre exacto de la(s) viñeta(s) del paquete. Intente ejecutar:

```{r eval=F}
vignette("base")
```

#### **5.4 Demos**

Al igual que las viñetas, muchos paquetes vienen con demostraciones interactivas integradas. Para enumerar todas las demostraciones disponibles en su sistema:

```{r eval=F}
demo("graphics", package = "graphics")
demo(package = .packages(all.available = TRUE))
```

### **Referencias**

* W. N. Venables, D. M. Smith, 2021. An Introduction to R [[Ver aquí]](https://cran.r-project.org/doc/manuals/r-release/R-intro.pdf)

  + Cap. 2: Simple manipulations; numbers and vectors
  + Cap. 3: Objects, their modes and attributes

* Colin Gillespie and Robin Lovelace, 2017. Efficient R Programming, A Practical Guide to Smarter Programming [[Ver aquí]](https://csgillespie.github.io/efficientR/)

  + Cap. 4: Efficient Workflow
  